from django.http import Http404
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from django.conf import settings
from pyjs.jsonrpc.django.jsonrpc import JSONRPCService,jsonremote
from random import randint

service = JSONRPCService()

def index(request,module=None,nocache=None):
    if module is None and nocache is None:
        return render_to_response(
                            'ajax_test/index.html',
                            {},
                            context_instance=RequestContext(request)
                        )
    elif isinstance(nocache,basestring) and nocache.lower() == 'nocache':
        # just do a redirect until I complete the mediagenerator backend,
        # at which point it will be necessary to query for the actual
        # filename and then redirect 
        return redirect_to_static(request,"%s.%s.html" % (module,nocache))
    else:
        raise Http404("File does not exist.")

def redirect_to_static(request,filename):
    to = "/%s/%s" % (settings.MEDIA_URL.strip('/'),filename)
    return redirect(to)

@jsonremote(service)
def getRandom(request):
    return [randint(0,i) for i in range(12)]
        